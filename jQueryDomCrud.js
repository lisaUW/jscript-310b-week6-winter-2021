// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

let a = $('<a/>',{
        id: 'cta'
    });

//$("a").append("Some appended text.");
//$(document.createElement('a'))

//.attr('id', 'cta').append("Buy Now!");

a.append("Buy Now!"); 

$("p:last").after(a);


// Access (read) the data-color attribute of the <img>,
let dataColor = $("img:first").attr("data-color");  

//$("#blue").attr("img");   
// log to the console
console.log(dataColor);

// Update the third <li> item ("Turbocharged"),
let thirdLi = $("ul li:nth-child(3)");

thirdLi.className = "highlight";
// set the class name to "highlight"

// Remove (delete) the last paragraph
$("p:last").remove(); 
// (starts with "Available for purchase now…")

// Create a listener on the "Buy Now!" link that responds to a click event.
a.addEventListener

let newText = "Added to cart"
a.click(function(){
	$(this).hide();
    $(this).after(newText)
});
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"
