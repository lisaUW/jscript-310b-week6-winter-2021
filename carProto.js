/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
let Car = class {
  constructor() {
    this.speed = 0;
  }

  accelerate(mph) {
    this.speed += mph;
  }

  brake(mph){
      this.speed -= mph;
  }
};

const car = new Car();

car.accelerate(60);
car.accelerate(60);

car.brake(60);

console.log("Speed is " + car.speed + " mph");
// output: "Rectangle"
/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
