/**
 * Toggles "done" class on <li> element
 */
$("li").toggleClass("done");

/**
 * Delete element when delete link clicked
 */

 //$("#demo").remove()

 const addDeleteEvent = function(){
   $(".delete").click(function(e){
   e.preventDefault()
  $(this).parent().remove();
});
 }
 addDeleteEvent();
/**
 * Adds new list item to <ul>
 */
const addListItem = function(e) {
  e.preventDefault();
  const text = $('input').val();
  let li = $('<li/>');
  let span = $('<span/>');
  span.html(text);
  let a = $('<a/>');
  a.addClass("delete");
  a.click(function(e){
   e.preventDefault()
  $(this).parent().remove();
});
  $(this).parent().remove();
  a.html("Delete")
  let ul = $('.today-list');
  li.append(span);
  li.append(a);
  ul.append(li);
};
  // add listener for add

$(".add-item").click(function(e){
  addListItem(e)
})





